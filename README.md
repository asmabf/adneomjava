# README #
This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This snippet takes a list of numbers and returns a list of sublists with a given parameter size. 
0.0.1-SNAPSHOT

### How do I get set up? ###
On the target folder, there's Partition.jar executable to be run on CMD/Bash using the following commandline: 
$<Path-of-the-jar-file>java -jar Partition.jar firstParameter secondParameter
Where 
	- firstParameter : A list of numbers separated by a comma ",". For example : 1,2,3,4
	- secondParameter : The size of each sublist to be generated. For example : 5


Maven manages the JUnit dependency.
Tests are to be executed using Eclipse IDE.

