package com.adneom.sujetmaison;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.adneom.sujetmaison.Utils;

import static org.junit.Assert.*;

public class TestUtils {

	@Test
	public void testPartition() {
		List<Integer> liste = Arrays.asList(1,2,3);
		List<List<Integer>> finalList = new ArrayList<>();
		finalList = Utils.partition(liste,2);
		ArrayList<List<Integer>> testList = new ArrayList<>();
		testList.add(Arrays.asList(1,2));
		testList.add(Arrays.asList(3));
		assertEquals(finalList, testList);
	}
	
	@Test
	public void testPartition2() {
		List<Integer> liste = Arrays.asList(1,2,3);
		List<List<Integer>> finalList = new ArrayList<>();
		finalList = Utils.partition(liste,2);
		ArrayList<List<Integer>> testList = new ArrayList<>();
		testList.add(Arrays.asList(1,2));
		assertNotEquals(finalList, testList);
	}
}
