package com.adneom.sujetmaison;

import java.util.ArrayList;
import java.util.List;

public class Utils {
	
	public static List<List<Integer>> partition(List<Integer> liste, int n) {
		// Liste des résultats
		List<List<Integer>> finalList = new ArrayList<>();
		if(n > 0) {
			int i = 0;
			// Liste temporaire (Sous Listes)
			List<Integer> tempList = new ArrayList<>();
			while(i <= liste.size()) {
				if(i%n == 0 || i >= liste.size()) {
					if(!tempList.isEmpty()) finalList.add(tempList);
					tempList = new ArrayList<>();
				}
				if(i >= liste.size()) break;
				tempList.add(liste.get(i));
				i++;
			}
		} else {
			System.out.println("partition sur 0 impossible!");
		}
		return finalList;
	}
	
}
