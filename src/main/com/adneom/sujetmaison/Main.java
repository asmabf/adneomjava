package com.adneom.sujetmaison;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Asma
 *
 */
public class Main {
	public static void main(String[] args) {
		String firstParameter = args[0];
		String secondParameter = args[1];
		
		String[] splitedFirstParameter = firstParameter.split(",");
		if (splitedFirstParameter == null || splitedFirstParameter.length == 0 ) {
			System.out.println("La saisie de la liste n'est pas bonne. Veuillez respecter le format [a,b,c,d x]");
			return ;
		}
			
		List<Integer> numbersList = new ArrayList<>();
		for (String item : splitedFirstParameter) {
			try {
				numbersList.add(Integer.valueOf(item));
			} catch (NumberFormatException e) {
				System.out.println("La saisie de la liste n'est pas bonne. Veuillez saisir des valeurs valides");
				return;
			}
		}
		
		Integer size = 0;
		try {
			size = Integer.valueOf(secondParameter);
		} catch (NumberFormatException e) {
			System.out.println("Le deuxi�me param�tre doit �tre un nombre");
			return;
		}
		
		
		List<List<Integer>> finalList = Utils.partition(numbersList, size);
		System.out.println("Le r�sultat obtenu est : " + finalList);
	}
}	
